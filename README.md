# Orientação a Objetos 2/2016
* Elisa Costa Lima
* Iasmin Santos Mendes

## EP3 - Ruby on Rails
Exercício Programa 3 da disciplina de Orientação a Objetos da Universidade de Brasília.

A aplicação consiste em um protótio de uma loja virtual de roupas femininas - nome ficitício Laços de Seda - com a funcionalidade de um Provador Online.

### Execução

* Baixe o arquivo do programa e abra a pasta do projeto pelo terminal.
* Execute **bundle install** para instalar as Gems aplicadas no projeto e suas dependências
* Execute: **rake db:migrate** para criar as colunas do banco de dados.
* Em sequida, rode o comando **rake db:seed** que salvará alguns valores padrões no banco de dados, inclusive a conta de Funcionário
* Inicialize o servidor por meio do comando **rails s**
* Abra um navegador, e acesse a url **localhost:3000**
* Inicialmente, a index estará vazia por não haver nenhuma roupa cadastrada, acesse a página de login com o e-mail **admin@lseda.com** e senha **admin1**
* Por meio dessa conta, será possível cadastrar modelos de roupas e seus respectivos estoques.
* As demais funcionalidades do site seguem de maneira intuitiva.
* Para fechar o servidor pressione **Ctrl+C**

*Link para o video: https://youtu.be/6SirAKxQZ90
