require 'test_helper'

class ClotesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @clote = clotes(:one)
  end

  test "should get index" do
    get clotes_url
    assert_response :success
  end

  test "should get new" do
    get new_clote_url
    assert_response :success
  end

  test "should create clote" do
    assert_difference('Clote.count') do
      post clotes_url, params: { clote: { category_id: @clote.category_id, code: @clote.code, description: @clote.description, model: @clote.model, price: @clote.price, size: @clote.size } }
    end

    assert_redirected_to clote_url(Clote.last)
  end

  test "should show clote" do
    get clote_url(@clote)
    assert_response :success
  end

  test "should get edit" do
    get edit_clote_url(@clote)
    assert_response :success
  end

  test "should update clote" do
    patch clote_url(@clote), params: { clote: { category_id: @clote.category_id, code: @clote.code, description: @clote.description, model: @clote.model, price: @clote.price, size: @clote.size } }
    assert_redirected_to clote_url(@clote)
  end

  test "should destroy clote" do
    assert_difference('Clote.count', -1) do
      delete clote_url(@clote)
    end

    assert_redirected_to clotes_url
  end
end
