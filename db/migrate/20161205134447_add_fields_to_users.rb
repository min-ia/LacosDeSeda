class AddFieldsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :name, :string
    add_column :users, :cpf, :string
    add_column :users, :public_place, :string
    add_column :users, :district, :string
    add_column :users, :number_house, :integer
    add_column :users, :complement, :string
    add_column :users, :cep, :string
    add_column :users, :city, :string
    add_column :users, :state, :string
    add_column :users, :bust, :float, :default => 0
    add_column :users, :waist, :float, :default => 0
    add_column :users, :hip, :float, :default => 0
    add_column :users, :employee, :string, :default => "n"
  end
end
