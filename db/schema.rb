# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161207114048) do

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "clotes", force: :cascade do |t|
    t.string   "model"
    t.float    "price"
    t.string   "description"
    t.integer  "category_id"
    t.string   "photo"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.index ["category_id"], name: "index_clotes_on_category_id"
  end

  create_table "measures", force: :cascade do |t|
    t.string   "size"
    t.float    "bust"
    t.float    "waist"
    t.float    "hip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stocks", force: :cascade do |t|
    t.integer  "amount"
    t.integer  "minimum"
    t.integer  "clote_id"
    t.integer  "measure_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["clote_id"], name: "index_stocks_on_clote_id"
    t.index ["measure_id"], name: "index_stocks_on_measure_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",  null: false
    t.string   "encrypted_password",     default: "",  null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,   null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "name"
    t.string   "cpf"
    t.string   "public_place"
    t.string   "district"
    t.integer  "number_house"
    t.string   "complement"
    t.string   "cep"
    t.string   "city"
    t.string   "state"
    t.float    "bust",                   default: 0.0
    t.float    "waist",                  default: 0.0
    t.float    "hip",                    default: 0.0
    t.string   "employee",               default: "n"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
