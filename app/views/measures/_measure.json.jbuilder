json.extract! measure, :id, :size, :bust, :waist, :hip, :created_at, :updated_at
json.url measure_url(measure, format: :json)