class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates_presence_of :name
  validates_presence_of :cpf
   with_options :allow_blank => true do |v|
     v.validates_length_of :cpf, :is => 11
     v.validates_numericality_of :cpf
     v.validates_uniqueness_of :cpf
   end
end
