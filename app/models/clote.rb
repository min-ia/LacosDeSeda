class Clote < ApplicationRecord
  has_many :stocks
  belongs_to :category
  has_attached_file :photo, :styles => { :medium => "300x300>", :thumb => "120x90#" }, :default_url => "/assets/imageNotAvailable_medium.jpg"
	 do_not_validate_attachment_file_type :photo
  
end
